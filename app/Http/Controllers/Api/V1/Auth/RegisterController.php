<?php

namespace App\Http\Controllers\Api\V1\Auth;

use App\Models\User;
use App\Http\Controllers\Controller;
use App\Http\Requests\Api\V1\Auth\LoginRequest;
use App\Http\Requests\Api\V1\Auth\RegisterRequest;

class RegisterController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param RegisterRequest $request
     * @return mixed
     */
    public function __invoke(RegisterRequest $request)
    {
        // ($request->file()) ? time().'_'.$request->file->getClientOriginalName() : '';
        $profile_img = '';
        if($request->file()){
            $file_name = time().'_'.$request->file->getClientOriginalName();
            $file_path = $request->file('file')->storeAs('uploads', $file_name, 'public');
            $profile_img = '/storage/' . $file_path;
        }

        $user = User::create([
            'name' => $request->name,
            'email' => $request->email,
            'password' => $request->password,
            'age' => $request->age,
            'profile_img' => $profile_img
        ]);
        
        return response()->json($user);

        //$credentials = $request->only(['email', 'password']);

        //$loginRequest = new LoginRequest($credentials);

        //return (new LoginController)($loginRequest);
    }
}
